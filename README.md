# CH CovidCertificate decoding Service

This service offers a REST interface for decoding data from 
images and RAW data of «CH Covid Certificates». COSE validation
is not yet implemented.

## TODO
- [X] add logging
- [ ] add javadoc documentation
- [ ] add REST proper exception handling
- [ ] add meaningful error messages on REST level
- [ ] improve image scanner reliability

## Goals:

1. Offer Webservice to decode HC1
   - Optional: Offer Webservice to decode barcode image scans (i.e. Smartphone camera)
1. Verify Validity of certificate (TBD)
2. Import data into HE

## how does it work

General Documentation for Swiss [CovidCert](https://github.com/admin-ch/CovidCertificate-Apidoc) and the [Verifier API](https://github.com/admin-ch/CovidCertificate-Apidoc/blob/main/open-api/api-doc.yaml).

Detailed information on decoding is sparse, but decoding the RAW data 
from an Covid Certificate (or «Digital Green Pass» as it is called in the 
EU) works roughly as follows:

- scan image with camera or RAW with a laser scanner
  - in case of image scan, use OpenCV to find and extract the QRCode
  - convert to raw data
- base45 decode ([IETF draft](https://datatracker.ietf.org/doc/html/draft-faltstrom-base45-03))
- unzip compressed data with deflate ([RFC 1951](https://datatracker.ietf.org/doc/html/rfc1951))
- parse COSE container ([RFC 8152](https://datatracker.ietf.org/doc/html/rfc8152)), get 3rd element
- dcode with CBOR ([RFC 8949](https://datatracker.ietf.org/doc/html/rfc8949))
- convert to JSON ([RFC 8259](https://datatracker.ietf.org/doc/html/rfc8259))
- The resulting data structure is defined as [JSON Schema (EU)](https://github.com/ehn-dcc-development/ehn-dcc-schema)
  - NOTE: `UVCI` is the unique identifier of a record.
  - CH Test data (barcode, intermediate formats and JSON) can be found in the eu [dgc-testdata/CH](https://github.com/eu-digital-green-certificates/dgc-testdata/tree/main/CH) repo

## other useful links

- [DGC CH Schema](https://github.com/admin-ch/CovidCertificate-Examples/tree/main/schema)
- [JSON Examples](https://github.com/admin-ch/CovidCertificate-Examples)

# running

`$ mvn spring-boot:run`

A Webservice should be available on port `8088`. You can change the port in `src/resources/application.properties`.

## End Points

- GET [/actuator](http://localhost:8088/actuator) health check
- GET [/swagger-ui/](http://localhost:8088/swagger-ui/) Swagger UI, test your web service
- POST [/api/v1/hc1_decode](http://localhost:8088/api/v1/hc1_decode) decode RAW HC1 data (usually obtained by scanning with a hand scanner)
- POST [/api/v1/image_decode](http://localhost:8088/api/v1/image_decode) decode an Image (usually obtained by smart-phone camera)

## Health Engine Datamodel

![Data Mdoel](doc/datamodel.png)

## how does it look?
![App](doc/COVID%20Certificate%20Check%20App.jpg)
![Paper](doc/202105.Covid-Zertifikat_Layouts-gesamt_entwurf.v1.0.jpg)


## CH / EU Architecture

![Architecture Covid Certificate](https://user-images.githubusercontent.com/319676/123532187-70ae1100-d70b-11eb-8977-c581b1c8dd5c.png)