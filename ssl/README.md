# self signed dev ssl cert for spring-boot

**FOR DEVELOPMENT ONLY** this cert helps during testing if you use client 
side html/javascript that requires ssl.

```
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*=--+#@@@@@%*---+%@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@*. -+-  :%@@+  -+=. =@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@. *%-#@  =@%  +@-+@= #@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@- .*+*=  #@@: :*=++. #@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@+      +@@@%:     =%@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@=   %@@@@@@:  =@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@%=   -+**++-   *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@%*=:                   .-+%@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@%+-::-*%@@@@@#=.   .-=*#%%@@@@@@@@%#*+-.    -*@@@@@@%+---=%@@@@@@@@@@@
@@@%%%@@%        :=+=:  .=*%@@@@@@@@@@@@@@@@@@@@@@#+:   -++-.       #@@@%@@@@@@
@*    .-:   =%#*:     =%@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#.     -+#-   .+-   .*@@@
@%+-:.      *@@@@:    +%@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#:    %@@@#       .-=#@@@
@@@@@@%+:.-*@@@#.       .-#@@@@@@@@@@@@@@@@@@@@@@@@#=.      .*@@@=:.:-*%@@@@@@@
@@@@@@@@@@@@@@@    =*#*.   .-+#@@@@@@@@@@@@@@@%#+:.   +***:   *@@@@@@@@@@@@@@@@
@@@@@@@@%%%@@@-   %@@@@          .:--==---:..         +@@@@:   %@@@%%@@@@@@@@@@
@@@@@#:          -@@@@=   =%@%#+=-:... ...:-=*#%%%*    @@@@#    :.   .-%@@@@@@@
@@@@@-   ..    .=@@@@@.  .@@@@@@@@@@@@@@@@@@@@@@@@@=   %@@@@+.     ..  :@@@@@@@
@@@@@@@%@@@@@@@@@@#+=-    @@@@@@@@@@@@@@@@@@@@@@@@@=   :*++#@@%%%@@@@@#@@@@@@@@
@@@@@@@@@@@@@@@@@.      :*@@@@@@@@@@@@@@@@@@@@@@@@@@-       %@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@#+++*#%@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#**==#@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
```

[Baeldung Tutorial](https://www.baeldung.com/spring-boot-https-self-signed-certificate)

`All Hail His Noodly Appendages` - random pastafarian

# steps

```bash
# super secure password: qwertz
keytool -genkeypair -alias pastafari -keyalg RSA -keysize 2048 \
    -storetype PKCS12 -keystore pastafari.p12 -validity 3650
# enter some meaningful random stuff!!
#keytool -genkeypair -alias pastafari -keyalg RSA -keysize \
#    2048 -keystore pastafari.jks -validity 3650
#keytool -importkeystore -srckeystore pastafari.jks \
#    -destkeystore pastafari.p12 -deststoretype pkcs12
```

copy the p12 file to `src/main/resources` and add the following entries 
to `application.properties`:

```
## ssl configuration, optional
server.ssl.enabled=true
# The format used for the keystore. It could be set to JKS in case it is a JKS file
server.ssl.key-store-type=PKCS12
# The path to the keystore containing the certificate
server.ssl.key-store=classpath:pastafari.p12
# The password used to generate the certificate
server.ssl.key-store-password=qwertz
# The alias mapped to the certificate
server.ssl.key-alias=pastafari
```