package net.wunderlin.covidcert.service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

import com.google.iot.cbor.*;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;

import org.springframework.stereotype.Service;

import ch.qos.logback.classic.Logger;
import nl.minvws.encoding.Base45;
import net.wunderlin.covidcert.exception.BarcodeDecodeException;
import net.wunderlin.covidcert.model.*;

import org.slf4j.LoggerFactory;

/**
 * Decode RAW HC1 String from Covid Certificate
 * 
 * (Also known as DGC in the EU: «Digital Green Certificate»). The 
 * swiss specifications ar as of 2021-06-29 compatible with the EU 
 * specifications. 
 * 
 * Decoding an HC1 encoded String involves several steps to get to the raw data.
 * 
 * - base45 decode (RFC: draft-faltstrom-base45-03)
 * - pkzip deflate (RFC: 1951)
 * - COSE decoding (RFC: 8152, CODE signature checks are not yet implemented)
 * - CBOR deocing the payload in the COSE message (RFC: 8949)
 * - convert to JSON (RFC: 5259)
 * - decode type definitions (TBD)
 * 
 * As for now, these API Methods can decode HC1 data from an image (eg. 
 * SmartPhone Camera) or from the raw string and (eg. laser scanner)
 * and will provide a wel defined data strcture `Cert` defined by CH/EU.
 * 
 * Schema definitions can be foudn here:
 * https://github.com/ehn-dcc-development/ehn-dcc-schema
 * 
 * Official CH Test data can be found here:
 * https://github.com/eu-digital-green-certificates/dgc-testdata/tree/main/CH
 * 
 */
@Service
public class BarcodeDecodeService {

    private static Logger logger = (Logger) LoggerFactory.getLogger(BarcodeDecodeService.class);

    // high level api ///////////////////////////////////////////////////////

    /**
     * Parse an HC1 String to Cert
     * 
     * This implementation expects an HC1 String defined here:
     * 
     * 
     * and will return a Cert object.
     * 
     * @param data HC1 as String
     * @return Cert
     * @throws Exception
     */
    public static Cert raw_to_cert(String data) throws Exception, BarcodeDecodeException {
        String removed_prefix = null;
        byte[] b45decoded = null;
        byte[] decompressed = null;
        Cert cert = null;
        ArrayList<String> arr = null;

        // remove prefix "HC1:"
        try {
            removed_prefix = BarcodeDecodeService.remove_prefix(data);
        } catch (Exception e) {
            String msg = "Failed to remove HC1 prefix:" + e.getMessage();
            logger.error(msg);
            logger.debug(data);
            throw new BarcodeDecodeException(1, msg);
        }

        // decode base45
        try {
            b45decoded = BarcodeDecodeService.b45decode(removed_prefix);
        } catch (Exception e) {
            String msg = "Base45 decode failed: " + e.getMessage();
            logger.error(msg);
            logger.debug(removed_prefix);
            throw new BarcodeDecodeException(2, msg);
        }

        // pkzip deflate
        try {
            decompressed = BarcodeDecodeService.decompress(b45decoded);
        } catch (Exception e) {
            String msg = "Failed to unzip content: " + e.getMessage();
            logger.error(msg);
            logger.debug(BarcodeDecodeService.bytesToHex(b45decoded));
            throw new BarcodeDecodeException(3, msg);
        }

		// extract COSE payload
        try {
            arr = BarcodeDecodeService.cose_to_array(decompressed);
        } catch (Exception e) {
            String msg = "COSE decoding failed: " + e.getMessage();
            logger.error(msg);
            logger.debug(BarcodeDecodeService.bytesToHex(decompressed));
            throw new BarcodeDecodeException(4, msg);
        }

        // decode CBOR content
        try {
            cert = BarcodeDecodeService.array_to_cert(arr);
        } catch (Exception e) {
            String msg = "CBOR decoding failed: " + e.getMessage();
            logger.error(msg);
            if (arr.size() > 1)
                logger.debug(arr.get(2));
            else 
                logger.debug("Element 2 missing in arr");
            throw new BarcodeDecodeException(5, msg);
        }
        
        logger.info("Successfully parsed Cert");
        logger.debug(cert.toString());

        return cert;
    }

    // helper functions /////////////////////////////////////////////////
	private static final byte[] HEX_ARRAY = "0123456789abcdef".getBytes(StandardCharsets.US_ASCII);
	public static String bytesToHex(byte[] bytes) {
		byte[] hexChars = new byte[bytes.length * 2];
		for (int j = 0; j < bytes.length; j++) {
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = HEX_ARRAY[v >>> 4];
			hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
		}
		return new String(hexChars, StandardCharsets.UTF_8);
	}

    // low level api ////////////////////////////////////////////////////

    // low level decodng functions, no need to used them directly, 
    // see high level api:
    // - raw_to_cert()
    // - image_to_cert()

    /**
     * remove prefix HC1: from the raw string
     * @param data
     * @return raw String with HC1: removed
     * @throws StringIndexOutOfBoundsException
     */
    public static String remove_prefix(String data) throws StringIndexOutOfBoundsException {
        String start = data.substring(0, 4);
        if (start.equals("HC1:")) {
            return data.substring(4);
        }
        
        return data;
    }

    /**
     * Base45 Decode
     * 
     * Base 45 decoding acording to IETF Draft draft-faltstrom-base45-03
     * 
     * @param data
     * @return
     */
    public static byte[] b45decode(String data) {
        return Base45.getDecoder().decode(data);
    }

    /**
     * pkzip deflate byte array
     * 
     * This method pkzip deflates a byte array according to RFC 1951. The 
     * internal method has a maximum buffer size of 8Kb. This should be enough
     * because the data length encoded inside a qrCode is quite small.
     * 
     * @param data
     * @return deflated byte array
     * @throws DataFormatException
     * @throws IOException
     */
    public static byte[] decompress(byte[] data) throws DataFormatException, IOException {
        // Decompress the bytes
        Inflater inflater = new Inflater();
        inflater.setInput(data, 0, data.length);
        byte[] result = new byte[8192]; // this is enough, barcode is limited to something around 2K
        int resultLength = inflater.inflate(result);
        inflater.end();
        byte[] out = Arrays.copyOfRange(result, 0, resultLength);
     
        return out;  
    }

    /**
     * extract COSE information
     * 
     * This is a hack. We are not validating the COSE signature.
     * We are just extracting the first 4 elements from the array,
     * element with index 3 contains the information we are after.
     * 
     * https://google.github.io/cbortree/releases/latest/apidocs/
     * @throws CborParseException
     */
    public static ArrayList<String> cose_to_array(byte[] data) throws CborParseException {
        ArrayList<String> ret = new ArrayList<String>();
        CborObject o = null;

        o = CborObject.createFromCborByteArray(data);

        if (o.getMajorType() == CborMajorType.ARRAY) {
            //System.out.println("Type: " + o.getMajorType());
            
            CborArray a = (CborArray) o;
            Iterator<CborObject> it = a.iterator();
            //int i = -1;
            while (it.hasNext()) {
                //i++;
                CborObject inner = it.next();
                // System.out.println(i + ": Type: " + inner.getMajorType());
                //System.out.println(inner.toString());
                if (inner.getMajorType() == CborMajorType.BYTE_STRING) {
                    CborByteString innerb = (CborByteString) inner;
                    CborObject inner_parsed = null;
                    try {
                        inner_parsed = CborObject.createFromCborByteArray(innerb.byteArrayValue());
                        ret.add(inner_parsed.toJsonString());
                    } catch (CborParseException e) {
                        // this is ugly but works, some data at 
                        // end of the COSE object wil be ignored
                        // we are not yet interested in this data, we do not verify
                        // a signature
                        //e.printStackTrace();
                        continue;
                    }
                } else {
                    // just add raw data if it's not a byte string
                    ret.add(inner.toString());
                }

            }

        } else {
            // no array at top level? we can't use the object. 
            // we expect at least an array with 4 elements
            ret = null;
        }

        return ret;
    }

    /**
     * parse date from iso 8601 String
     * 
     * if the date is missing time, "T00:00:00+00:00" will appended before parsing.
     * 
     * @param strdate
     * @return
     */
    public static Date parse_str_date(String strdate) {
        Date dt = null;
        if (strdate.length() == 10) { // date only, add 0 time
            strdate += "T00:00:00+00:00";
        }
        dt = Date.from(ZonedDateTime.parse(strdate).toInstant());
        return dt;
    }

    /**
     * Exctract payload from COSE container
     * 
     * This method will extract the relevant payload from the 3rd 
     * cose array element. It will then be CBOR decoded and a Cert object 
     * will be populated.
     * 
     * If the COSE elemnts are encrypted, this method will fail. You must make 
     * sure to first decrypt the content.
     * 
     * @param arr
     * @return
     * @throws Exception
     */
    public static Cert array_to_cert(ArrayList<String> arr) throws Exception {
        Cert c = Cert.factory();
		Configuration conf = Configuration.defaultConfiguration();

        String json = arr.get(2);
		Object dst_doc = conf.jsonProvider().parse(json);
		String t = "";

		try {
			JsonPath.read(dst_doc, "$.-260.1.t");
			t = "t";
		} catch (Exception e1) {
			try {
				JsonPath.read(dst_doc, "$.-260.1.r");
				t = "r";
			} catch (Exception e2) {
				try {
					JsonPath.read(dst_doc, "$.-260.1.v");
					t = "v";
				} catch (Exception e3) {
					// no type found, this isn't right
					//System.out.println("");
					//System.out.println(result_json);
					//System.out.println(JsonPath.read(document, "$.JSON").toString());
					throw new Exception("No data array found in json");
				}
			}
		}

        // populate dob and person
        Name n = new Name();
        n.setFn(JsonPath.read(dst_doc, "$.-260.1.nam.fn").toString());
        n.setFnt(JsonPath.read(dst_doc, "$.-260.1.nam.fnt").toString());
        n.setGn(JsonPath.read(dst_doc, "$.-260.1.nam.gn").toString());
        n.setGnt(JsonPath.read(dst_doc, "$.-260.1.nam.gnt").toString());
        c.setNam(n);
        String strdt = JsonPath.read(dst_doc, "$.-260.1.dob").toString();
        c.setDob(BarcodeDecodeService.parse_str_date(strdt));
        c.setVer(JsonPath.read(dst_doc, "$.-260.1.ver").toString());

        // now, parse the result
        Generic ref = null;
        if (t == "v") {
            Vaccinated inst = new Vaccinated();
            ref = inst;
            inst.setVp(JsonPath.read(dst_doc, "$.-260.1.v[0].vp").toString());
            inst.setMp(JsonPath.read(dst_doc, "$.-260.1.v[0].mp").toString());
            inst.setMa(JsonPath.read(dst_doc, "$.-260.1.v[0].ma").toString());
            inst.setDn(Integer.parseInt(JsonPath.read(dst_doc, "$.-260.1.v[0].dn").toString()));
            inst.setSd(Integer.parseInt(JsonPath.read(dst_doc, "$.-260.1.v[0].sd").toString()));
            inst.setDt(BarcodeDecodeService.parse_str_date(JsonPath.read(dst_doc, "$.-260.1.v[0].dt").toString()));
            c.v.add(inst);
        } else if (t == "t") {
            Tested inst = new Tested();
            ref = inst;
            inst.setTt(JsonPath.read(dst_doc, "$.-260.1.t[0].tt").toString());
            inst.setTr(JsonPath.read(dst_doc, "$.-260.1.t[0].tr").toString());
            inst.setMa(JsonPath.read(dst_doc, "$.-260.1.t[0].ma").toString());
            inst.setTc(JsonPath.read(dst_doc, "$.-260.1.t[0].tc").toString());
            inst.setSc(BarcodeDecodeService.parse_str_date(JsonPath.read(dst_doc, "$.-260.1.t[0].sc").toString()));

            // optional
            try {
                inst.setDr(BarcodeDecodeService.parse_str_date(JsonPath.read(dst_doc, "$.-260.1.t[0].dr").toString()));
            } catch (PathNotFoundException e) {}
            try {
                inst.setNm(JsonPath.read(dst_doc, "$.-260.1.t[0].nm").toString());
            } catch (PathNotFoundException e) {}

            c.t.add(inst);
        } else if (t == "r") {
            Recovered inst = new Recovered();
            ref = inst;
            inst.setDu(BarcodeDecodeService.parse_str_date(JsonPath.read(dst_doc, "$.-260.1.r[0].du").toString()));
            inst.setDf(BarcodeDecodeService.parse_str_date(JsonPath.read(dst_doc, "$.-260.1.r[0].df").toString()));
            inst.setFr(BarcodeDecodeService.parse_str_date(JsonPath.read(dst_doc, "$.-260.1.r[0].fr").toString()));
            c.r.add(inst);
        }

        // generic values
        ref.setTg(JsonPath.read(dst_doc, "$.-260.1."+t+"[0].tg").toString());
        ref.setCo(JsonPath.read(dst_doc, "$.-260.1."+t+"[0].co").toString());
        ref.setIs(JsonPath.read(dst_doc, "$.-260.1."+t+"[0].is").toString());
        ref.setCi(JsonPath.read(dst_doc, "$.-260.1."+t+"[0].ci").toString());

        return c;
    }
}
