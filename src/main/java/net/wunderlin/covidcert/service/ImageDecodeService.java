package net.wunderlin.covidcert.service;

import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.springframework.stereotype.Service;

/**
* Extracts DGC from image
* 
* This decoder expects a fairly good BImage containing a barcode. It is
* just a POC for testing the decode. Better Barcode extraction from the 
* image must be implemented to make it usable with smartphone cameras.
* 
* Better yet, use native barcode encoding mechanisms from the smart phone
* camera in the first place and use `this.image_to_cert()`.
*/
@Service
public class ImageDecodeService {

    public static String decode_file(File qrCodeimage) throws IOException {
        BufferedImage bufferedImage = ImageIO.read(qrCodeimage);
        return ImageDecodeService.decode(bufferedImage);
    }

    public static String decode_byte_array(byte[] data) throws IOException {
        InputStream is = new ByteArrayInputStream(data);
        BufferedImage bi = ImageIO.read(is);
        return ImageDecodeService.decode(bi);
    }

    private static String decode(BufferedImage bufferedImage) {
        LuminanceSource source = new BufferedImageLuminanceSource(bufferedImage);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

        try {
            Result result = new MultiFormatReader().decode(bitmap);
            return result.getText();
        } catch (NotFoundException e) {
            System.out.println("There is no QR code in the image");
            return null;
        }
    }
}
