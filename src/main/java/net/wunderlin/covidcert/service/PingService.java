package net.wunderlin.covidcert.service;

import org.springframework.stereotype.Service;

@Service
public class PingService {

    public PingService() {}

    public String response() {
        return "pong";
    }

}
