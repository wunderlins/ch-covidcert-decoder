package net.wunderlin.covidcert;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"net.wunderlin.covidcert"})
public class DecoderApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(DecoderApplication.class, args);
	}

}
