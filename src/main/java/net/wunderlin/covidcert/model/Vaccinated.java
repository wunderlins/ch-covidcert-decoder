package net.wunderlin.covidcert.model;

import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class Vaccinated extends Generic {
    // vaccine or prophylaxis
    // Value Sets for Digital Green Certificates. version 1.0, 2021-04-16, section 2.2
    // Type string according vaccine-prophylaxis in "DGC.ValueSets.schema.json"
    // SHALL contain one of the values "prophylaxis_code" defined in ./cumulated/covid-19-vaccines.json
    //"vp": "1119349007",
    private String vp;

    // vaccine medicinal product
    // Value Sets for Digital Green Certificates. version 1.0, 2021-04-16, section 2.3
    // Type string according vaccine-medicinal-product in "DGC.ValueSets.schema.json"
    // SHALL contain one of the values "code" defined in ./cumulated/covid-19-vaccines.json
    //"mp": "EU/1/20/1528",
    private String mp;

    // Marketing Authorization Holder - if no MAH present, thenmanufacturer
    // Value Sets for Digital Green Certificates. version 1.0, 2021-04-16, section 2.3
    // Type string vaccine-mah-manf in "DGC.ValueSets.schema.json"
    // SHALL contain one of the values "auth_holder_code" defined in ./cumulated/covid-19-vaccines.json
    // "ma": "ORG-100030215",
    private String ma;

    // Dose number
    // Int min 1, max 9 according dose_posint in "DGC.Core.Types.schema.json"
    // "dn": 1,
    private int dn;

    // Total Series of Doses
    // Int min 1, max 9 according dose_posint in "DGC.Core.Types.schema.json"
    //"sd": 2,
    private int sd;

    // Date of Vaccination, ISO 8601
    // String containing date according dt in "DGC.Types.schema.json"
    // "dt": "2021-04-22",
    private Date dt;
}
