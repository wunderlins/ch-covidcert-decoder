package net.wunderlin.covidcert.model;

import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class Recovered extends Generic {
    // Date of First Positive Test Result, ISO 8601
    // String with date content according fr in "DGC.Types.schema.json" 
    // "fr": "2021-04-22",
    private Date fr;

    // Certificate Valid From, ISO 8601
    // String with date content according df in "DGC.Types.schema.json" 
    //"df": "2021-05-01",
    private Date df;
    
    // Certificate Valid Until, ISO 8601
    // String with date content according du in "DGC.Types.schema.json"
    // MAX duration is df + 180 days according https://ec.europa.eu/health/sites/health/files/ehealth/docs/digital-green-certificates_dt-specifications_en.pdf
    //"du": "2021-10-21",
    private Date du;
}
