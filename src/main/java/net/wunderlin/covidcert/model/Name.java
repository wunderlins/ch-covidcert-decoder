package net.wunderlin.covidcert.model;

import lombok.Data;

@Data
public class Name {
    // Family name
    // String max length 50 chars
    //"fn": "d'Arsøns - van Hale",
    private String fn;

    // Given name
    // String max length 50 chars
    //"gn": "François-Joan",
    private String gn;

    // Standardised family name
    // String max length 50 chars with pattern "^[A-Z<]*$"
    //"fnt": "DARSONS<VAN<HALEN",
    private String  fnt;

    // Standardised given name
    // String max length 50 chars with pattern "^[A-Z<]*$"
    //"gnt": "FRANCOIS<JOAN"
    private String gnt;
}
