package net.wunderlin.covidcert.model;

public enum CertType {
    UNKNOWN,
    VACCINATED,
    TESTED,
    RECOVERED
}
