package net.wunderlin.covidcert.model;

import lombok.Data;

@Data
public abstract class Generic {
    // disease or agent targeted
    // Value Sets for Digital Green Certificates. version 1.0, 2021-04-16, section 2.1
    // Type string according disease-agent-targeted in "DGC.ValueSets.schema.json"
    //"tg": "840539006",
    private String tg;

    // Country of Vaccination, ISO 3166 (where possible)
    // String with pattern [A-Z]{1,10} according country_vt in "DGC.Core.Types.schema.json"
    //"co": "CH",
    private String co;

    // Certificat Issuer
    // String with max length 50 according issuer in "DGC.Core.Types.schema.json"
    // In CH the value shall be fixed to BAG/OFSP/FOPH --> do not forget the translations/languages (EN/DE/FR/IT)
    // "is": "Bundesamt für Gesundheit (BAG)",
    private String is;

    // Unique Certificate Identifier: UVCI
    // Unique string maxLength 50 according certification_id in "DGC.Core.Types.schema.json"
    // Example according definition in https://ec.europa.eu/health/sites/health/files/ehealth/docs/vaccination-proof_interoperability-guidelines_en.pdf ANNEX 2
    // Additional Info in the eHN Disscusion https://github.com/ehn-digital-green-development/ehn-dgc-schema/issues/15
    //"ci": "01:CH:PlA8UWS60Z4RZVALl6GAZ:12"
    private String ci;
}
