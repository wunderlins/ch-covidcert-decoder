package net.wunderlin.covidcert.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * CH Covid Certificate Data Mdoel
 * 
 * Schema: https://github.com/admin-ch/CovidCertificate-Examples
 */
@Data
public class Cert {
    private String ver;
    private Name nam;
    private Date dob;

    public List<Vaccinated> v;
    public List<Tested>     t;
    public List<Recovered>  r;

    public static Cert factory() {
        Cert cert = new Cert();
        cert.v = new ArrayList<Vaccinated>();
        cert.t = new ArrayList<Tested>();
        cert.r = new ArrayList<Recovered>();

        return cert;
    }

    public Boolean hasVaccinated() {return (this.v.size() > 0);}
    public Boolean hasTested()     {return (this.t.size() > 0);}
    public Boolean hasRecovered()  {return (this.r.size() > 0);}

}
