package net.wunderlin.covidcert.model;

import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class Tested extends Generic {
    // Type of test
    // Value Sets for Digital Green Certificates. version 1.0, 2021-04-16, section 2.7
    // Type string, according tt in "DGC.Types.schema.json"
    //"tt": "LP217198-3",
    private String tt;

    // Test Result
    // "EU eHealthNetwork: Value Sets for Digital Green Certificates. version 1.0, 2021-04-16, section 2.9
    // Type string, required code according test-reslut in "DGC.ValueSets.schema.json"
    //"tr": "260415000",
    private String tr;

    // Test Manufacturer
    // Value Sets for Digital Green Certificates. version 1.0, 2021-04-16, section 2.8
    // IF key tt is with the value LP217198-3 then the ma field SHALL be set.
    // Type string, SHALL contain one of the values "manufacturer_code_eu" defined in ./cumulated/covid-19-tests.json
    //"ma": "1232",
    private String ma;

    // Date/Time of Sample Collection
    // Type string, format date-time ISO 8601 according sc in "DGC.Types.schema.json"
    //"sc": "2021-04-13T14:20:00+00:00",
    private Date sc;

    // Date/Time of Test Result
    // Type string, format date-time ISO 8601 according dr in "DGC.Types.schema.json"
    //"dr": "2021-04-13T14:40:01+00:00",
    private Date dr;

    // Testing Centre
    // String with a maxLength of 50 according tc in "DGC.Types.schema.json"
    //"tc": "Amavita Apotheke Bahnhof Bern",  
    private String tc;

    private String nm;
}
