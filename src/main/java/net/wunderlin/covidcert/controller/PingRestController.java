package net.wunderlin.covidcert.controller;

import net.wunderlin.covidcert.service.PingService;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * This is a REST «hello world»
 * 
 * check if spring-boot and stuff compiles and works.
 */
//@RestController --> disabled
public class PingRestController {

	@Autowired
	PingService pingService;

  @RequestMapping(method = RequestMethod.GET, path = "/api/v1/ping")
  public ResponseEntity<String> getPing() {
    return ResponseEntity.ok(pingService.response());
  }

}