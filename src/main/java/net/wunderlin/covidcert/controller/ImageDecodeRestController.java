package net.wunderlin.covidcert.controller;

import java.io.IOException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponses;
import net.wunderlin.covidcert.model.Cert;
import net.wunderlin.covidcert.service.*;
import io.swagger.annotations.ApiResponse;

@RestController
public class ImageDecodeRestController {

  @RequestMapping(method = RequestMethod.POST, path = "/api/v1/image_decode")
  @ApiResponses(value = { 
    @ApiResponse(code = 200, message = "Sucsessfuly decoded"),
    @ApiResponse(code = 500, message = "Failed to decode") })
  public ResponseEntity<Object> postImageDecode(
    @ApiParam(value = "jpeg/png image data", defaultValue="")
    @RequestParam MultipartFile qrcode
  ) throws IOException, Exception {
    Cert cert = Cert.factory();
    String hc1 = ImageDecodeService.decode_byte_array(qrcode.getBytes());

    if (hc1 == null || hc1.length() == 0)
      throw new Exception("Barcode decoding failed.");

    try {
        cert = BarcodeDecodeService.raw_to_cert(hc1);
    } catch (Exception e) {
        //System.out.println("Error while decoding");
        return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    return ResponseEntity.ok(cert);

  }

}