package net.wunderlin.covidcert.controller;

import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Display a static index page
 */
public class IndexPageController {
    @RequestMapping("/")
    public String index() {
        return "index.html";
    } 
}
