package net.wunderlin.covidcert.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.Operation;
import net.wunderlin.covidcert.exception.BarcodeDecodeException;
import net.wunderlin.covidcert.model.Cert;
import net.wunderlin.covidcert.service.BarcodeDecodeService;
import io.swagger.annotations.ApiResponse;

@RestController
public class Hc1DecodeRestController {

  @RequestMapping(method = RequestMethod.POST, path = "/api/v1/hc1_decode")
  @Operation(summary = "Decode «HC1:...» data for DGC")
  @ApiResponses(value = { 
    @ApiResponse(code = 200, message = "Sucsessfuly decoded"),
    @ApiResponse(code = 500, message = "Failed to decode") })
  public ResponseEntity<Cert> postHc1Decode(
    @ApiParam(value = "HC1:NCFK...VVV8P4", defaultValue="")
    @RequestParam String hc1
  ) throws BarcodeDecodeException, Exception {

    Cert cert = Cert.factory();
    cert = BarcodeDecodeService.raw_to_cert(hc1);

    return ResponseEntity.ok(cert);
  }

}