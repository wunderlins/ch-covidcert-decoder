package net.wunderlin.covidcert.exception;

public class NotImplementedException extends Exception {

    public NotImplementedException(String string) {
        super(string);
    }

}
