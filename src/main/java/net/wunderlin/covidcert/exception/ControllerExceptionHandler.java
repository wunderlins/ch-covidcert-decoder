package net.wunderlin.covidcert.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice()
public class ControllerExceptionHandler {
    
    /*
    @ExceptionHandler(BarcodeDecodeException.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorMessage globalExceptionHandler(Exception ex, WebRequest request) {
        ErrorMessage message = new ErrorMessage(ex);
        System.out.println("ERROR:" + ex.getMessage());
        return message;
    }
    */

    @ExceptionHandler(value = RuntimeException.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody ResponseEntity<ErrorMessage> handleException(final RuntimeException ex, WebRequest request) {
        ErrorMessage message = new ErrorMessage(ex);
        System.out.println("handleException: " + ex.getMessage());
        
        return new ResponseEntity<ErrorMessage>(message, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody ResponseEntity<ErrorMessage> handleException(final Exception ex, WebRequest request) {
        ErrorMessage message = new ErrorMessage(ex);
        System.out.println("handleException: " + ex.getMessage());
        
        return new ResponseEntity<ErrorMessage>(message, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = BarcodeDecodeException.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody ResponseEntity<ErrorMessage> handleBarcodeDecodeException(final Exception ex, WebRequest request) {
        ErrorMessage message = new ErrorMessage(ex);
        System.out.println("handleBarcodeDecodeException: " + ex.getMessage());
        
        return new ResponseEntity<ErrorMessage>(message, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}