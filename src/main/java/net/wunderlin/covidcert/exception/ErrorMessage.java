package net.wunderlin.covidcert.exception;

import java.util.Date;

public class ErrorMessage {
    Date date;
    String message;
    int code;
    
    public ErrorMessage(Exception e) {
        this.date = new Date();
        this.message = e.getMessage();
        this.code = -1;
        if (e instanceof BarcodeDecodeException) {
            this.code = ((BarcodeDecodeException) e).code;
        }
    }
}
