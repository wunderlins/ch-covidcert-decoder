package net.wunderlin.covidcert.exception;

public class BarcodeDecodeException extends Exception {
    public int code;
    public BarcodeDecodeException(int code, String string) {
        super(string);
        this.code = code;
    }
}

