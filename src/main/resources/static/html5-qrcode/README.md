# html5-qrcode

This is the compiled/minified version of the following Project:

- [github](https://github.com/mebjas/html5-qrcode)
- [demo](https://blog.minhazav.dev/research/html5-qrcode.html)
- [Tutorial](https://minhazav.medium.com/qr-and-barcode-scanner-using-html-and-javascript-2cdc937f793d)

License Apache 2.0, (c) 2020 MINHAZ <minhazav@gmail.com>