package net.wunderlin.covidcert;

import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.zip.DataFormatException;

import com.google.iot.cbor.CborParseException;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;

import org.apache.commons.io.FileUtils;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;

import net.wunderlin.covidcert.service.PingService;
import net.wunderlin.covidcert.exception.BarcodeDecodeException;
import net.wunderlin.covidcert.model.Cert;
import net.wunderlin.covidcert.model.CertType;
import net.wunderlin.covidcert.model.Generic;
import net.wunderlin.covidcert.model.Recovered;
import net.wunderlin.covidcert.model.Tested;
import net.wunderlin.covidcert.service.BarcodeDecodeService;
import net.wunderlin.covidcert.service.ImageDecodeService;

//@SpringBootTest // -> make shit fast by not using spring boot on api level
class DecoderApplicationTests {

	// private static Logger LOGGER = LoggerFactory.getLogger(DecoderApplicationTests.class);

	public DecoderApplicationTests() {
		// silence debug log output from JsonPath
		LoggerContext logContext = (LoggerContext) LoggerFactory.getILoggerFactory();
		ch.qos.logback.classic.Logger log = logContext.getLogger("com.jayway.jsonpath.internal.path.CompiledPath");
		log.setLevel(Level.INFO); 
	}	

	String testFilePath(String fileName) {
		Path resourceDirectory = Paths.get("src", "test", "resources", "test_data", fileName);
		return resourceDirectory.toFile().getAbsolutePath();
	}

	@Test
	void contextLoads() {
	}

	@Test
	void ping() {
		PingService pingService = new PingService();
		assertEquals(pingService.response(), "pong");
	}

	Boolean compare_json(String path, Object dsrc, Object ddst) {
		String src = JsonPath.read(dsrc, "$.JSON."+path).toString();
		String dst = JsonPath.read(ddst, "$.-260.1."+path).toString();
		//System.out.println(src + " = " + dst);
		return src.equals(dst);
	}

	String read_resource(String file_name) throws IOException {
		// read file
		File file = new File(this.testFilePath(file_name));
		String data = null;;

		data = FileUtils.readFileToString(file, "UTF-8");

		return data;
	}

	@Test
	void prefix_exception() {
		Throwable throwable =  assertThrows(Throwable.class, () -> {
			String removed_prefix = BarcodeDecodeService.remove_prefix("");
		});
		assertEquals(StringIndexOutOfBoundsException.class, throwable.getClass());

		String res = BarcodeDecodeService.remove_prefix("HC1:aaa");
		assertEquals(res, "aaa");
	}

	void decode(int n) throws IOException, DataFormatException, CborParseException {

		// read file
		String data = this.read_resource(n + ".json");
		System.out.print(".");

		// extract data from example json file
		// https://github.com/json-path/JsonPath
		Configuration conf = Configuration.defaultConfiguration();
		
		// get result data for comparison from example json
		Object document = conf.jsonProvider().parse(data);
		//String cbor = JsonPath.read(document, "$.CBOR");
		String cose = JsonPath.read(document, "$.COSE");
		String compressed = JsonPath.read(document, "$.COMPRESSED");
		String base45 = JsonPath.read(document, "$.BASE45");
		String prefix = JsonPath.read(document, "$.PREFIX");
		//System.out.println(prefix);

		// remove prefix "HC1:" fro mraw string
		String removed_prefix = BarcodeDecodeService.remove_prefix(prefix);
		//System.out.println(removed_prefix);
		assertEquals(removed_prefix, base45);
		System.out.print(".");

		// base45 decode and compare string representation
		byte[] b45decoded = BarcodeDecodeService.b45decode(removed_prefix);
		String b45decoded_s = BarcodeDecodeService.bytesToHex(b45decoded);
		//System.out.println(b45decoded_s);
		assertEquals(b45decoded_s, compressed);
		System.out.print(".");

		// zip decompress (deflate)
		byte[] decompressed = BarcodeDecodeService.decompress(b45decoded);

		String decompressed_s = BarcodeDecodeService.bytesToHex(decompressed);
		assertEquals(decompressed_s, cose);
		System.out.print(".");

		// get json from object
		ArrayList<String> s = BarcodeDecodeService.cose_to_array(decompressed);
		assertEquals(s.size(), 3);
		String result_json = s.get(2);
		System.out.print(".");


		// compare result with json 
		Object dst_doc = conf.jsonProvider().parse(result_json);
		//System.out.println(result_json);

		// compare name
		assertTrue(this.compare_json("nam.fn", document, dst_doc));
		assertTrue(this.compare_json("nam.gn", document, dst_doc));
		assertTrue(this.compare_json("nam.fnt", document, dst_doc));
		assertTrue(this.compare_json("nam.gnt", document, dst_doc));
		System.out.print(".");

		// compare date of birth and version
		assertTrue(this.compare_json("dob", document, dst_doc));
		assertTrue(this.compare_json("ver", document, dst_doc));
		System.out.print(".");

		// compare result array index 0, we need to probe the type which is one of v/t/r
		String t = "";
		CertType tp = CertType.UNKNOWN;

		try {
			JsonPath.read(dst_doc, "$.-260.1.t");
			tp = CertType.TESTED;
			t = "t";
		} catch (Exception e1) {
			try {
				JsonPath.read(dst_doc, "$.-260.1.r");
				tp = CertType.RECOVERED;
				t = "r";
			} catch (Exception e2) {
				try {
					JsonPath.read(dst_doc, "$.-260.1.v");
					tp = CertType.VACCINATED;
					t = "v";
				} catch (Exception e3) {
					// no type found, this isn't right
					//System.out.println("");
					//System.out.println(result_json);
					//System.out.println(JsonPath.read(document, "$.JSON").toString());
					//throw e3;
				}
			}
		}
		assertNotEquals(tp, CertType.UNKNOWN);
		System.out.print(".");

		//System.out.println(result_json);
		//System.out.println(JsonPath.read(document, "$.JSON").toString());

		// find keys in type specifiv element v/r/t
		HashMap keys = JsonPath.read(document, "$.JSON." + t + "[0]"); 

		// common keys
		if (keys.containsKey("tg"))
			assertTrue(this.compare_json(t + "[0].tg", document, dst_doc));
		if (keys.containsKey("co"))
			assertTrue(this.compare_json(t + "[0].co", document, dst_doc));
		if (keys.containsKey("is"))
			assertTrue(this.compare_json(t + "[0].is", document, dst_doc));
		if (keys.containsKey("ci"))
			assertTrue(this.compare_json(t + "[0].ci", document, dst_doc));
			System.out.print(".");

		// vaccinated
		if ( tp == CertType.VACCINATED) {
			if (keys.containsKey("vp"))
				assertTrue(this.compare_json(t + "[0].vp", document, dst_doc));
			if (keys.containsKey("mp"))
				assertTrue(this.compare_json(t + "[0].mp", document, dst_doc));
			if (keys.containsKey("ma"))
				assertTrue(this.compare_json(t + "[0].ma", document, dst_doc));
			if (keys.containsKey("dn"))
				assertTrue(this.compare_json(t + "[0].dn", document, dst_doc));
			if (keys.containsKey("sd"))
				assertTrue(this.compare_json(t + "[0].sd", document, dst_doc));
			if (keys.containsKey("dt"))
				assertTrue(this.compare_json(t + "[0].dt", document, dst_doc));
		}

		// tested
		else if (tp == CertType.TESTED) {
			if (keys.containsKey("tt"))
				assertTrue(this.compare_json(t + "[0].tt", document, dst_doc));
			if (keys.containsKey("nm"))
				assertTrue(this.compare_json(t + "[0].nm", document, dst_doc));
			if (keys.containsKey("ma"))
				assertTrue(this.compare_json(t + "[0].ma", document, dst_doc));
			if (keys.containsKey("sc"))
				assertTrue(this.compare_json(t + "[0].sc", document, dst_doc));
			if (keys.containsKey("tr"))
				assertTrue(this.compare_json(t + "[0].tr", document, dst_doc));
			if (keys.containsKey("tc"))
				assertTrue(this.compare_json(t + "[0].tc", document, dst_doc));
		}

		// recovered
		else if (tp == CertType.RECOVERED) {
			if (keys.containsKey("fr"))
				assertTrue(this.compare_json(t + "[0].fr", document, dst_doc));

			if (keys.containsKey("df"))
				assertTrue(this.compare_json(t + "[0].df", document, dst_doc));

			if (keys.containsKey("du"))
				assertTrue(this.compare_json(t + "[0].du", document, dst_doc));
		}

		// unknown
		else {
			assertEquals(1, 0);
		}

		System.out.println(".");
	}

	@Test
	void decode_12() throws IOException, DataFormatException, CborParseException {
		// test all 12 test sets
		for (int i=1; i<13; i++) {
			System.out.print("Decoding: " + i);
			this.decode(i);
		}
	}

	String get_hc1(int n) throws IOException {
		String data = this.read_resource(n + ".json");
		Configuration conf = Configuration.defaultConfiguration();
		Object document = conf.jsonProvider().parse(data);
		return JsonPath.read(document, "$.PREFIX").toString();
	}

	void decode_and_compare_image(int n) throws IOException {
		// read file
		File file = new File(this.testFilePath(n + ".png"));
		String data = null;

		data = ImageDecodeService.decode_file(file);

		assertEquals(data, this.get_hc1(n));
	}

	@Test
	void decode_qrcode_file() throws IOException {
		for(int i=1; i<13; i++) {
			System.out.println("Image: " + i);
			decode_and_compare_image(i);
		}
	}

	// helper methods to compare the parsed structures to the original 
	// JSON data in the test files
	void compare_string(Object document, String path, String result) {
		String expect = JsonPath.read(document, "$.JSON." + path);
		assertEquals(expect, result);
	}

	void compare_int(Object document, String path, int result) {
		int expect = JsonPath.read(document, "$.JSON." + path);
		assertEquals(expect, result);
	}

	void compare_date(Object document, String path, Date result) {
		String expect_str = JsonPath.read(document, "$.JSON." + path);
		Date expect = BarcodeDecodeService.parse_str_date(expect_str);
		assertEquals(expect, result);
	}

	void compare_cert_common(Object document, Cert cert) {
		// compare generic values
		this.compare_string(document, "ver",     cert.getVer());
		this.compare_date  (document, "dob",     cert.getDob());
		this.compare_string(document, "nam.fn",  cert.getNam().getFn());
		this.compare_string(document, "nam.fnt", cert.getNam().getFnt());
		this.compare_string(document, "nam.gn",  cert.getNam().getGn());
		this.compare_string(document, "nam.gnt", cert.getNam().getGnt());
	}

	void compare_cert_generic(Object document, Generic g) {
		String prefix = "v";
		if (g instanceof Tested) prefix = "t";
		if (g instanceof Recovered) prefix = "r";
		// compare type specific common values
		this.compare_string(document, prefix+"[0].tg", g.getTg());
		this.compare_string(document, prefix+"[0].co", g.getCo());
		this.compare_string(document, prefix+"[0].is", g.getIs());
		this.compare_string(document, prefix+"[0].ci", g.getCi());
	}

	void compare_cert_specific(Object document, Cert cert) {
		if (cert.hasVaccinated()) {
			this.compare_string(document, "v[0].vp", cert.v.get(0).getVp());
			this.compare_string(document, "v[0].mp", cert.v.get(0).getMp());
			this.compare_string(document, "v[0].ma", cert.v.get(0).getMa());
			this.compare_int   (document, "v[0].dn", cert.v.get(0).getDn());
			this.compare_int   (document, "v[0].sd", cert.v.get(0).getSd());
			this.compare_date  (document, "v[0].dt", cert.v.get(0).getDt());
		}

		if (cert.hasTested()) {
			this.compare_string(document, "t[0].tt", cert.t.get(0).getTt());
			this.compare_string(document, "t[0].nm", cert.t.get(0).getNm());
			this.compare_date  (document, "t[0].sc", cert.t.get(0).getSc());
			this.compare_string(document, "t[0].tr", cert.t.get(0).getTr());
			this.compare_string(document, "t[0].tc", cert.t.get(0).getTc());
		}

		if (cert.hasRecovered()) {
			this.compare_date  (document, "r[0].fr", cert.r.get(0).getFr());
			this.compare_date  (document, "r[0].df", cert.r.get(0).getDf());
			this.compare_date  (document, "r[0].du", cert.r.get(0).getDu());	
		}
	}

	@Test 
	void decode_vacccinated_to_cert() throws IOException, Exception {
		Configuration conf = Configuration.defaultConfiguration();

		// vaccine
		String data = this.read_resource("1.json");
		Object document = conf.jsonProvider().parse(data);
		String prefix = JsonPath.read(document, "$.PREFIX");		
		
		// instantiate structured certificate Data
		Cert cert = BarcodeDecodeService.raw_to_cert(prefix);

		// compare Cert content to original json
		this.compare_cert_common(document, cert);
		this.compare_cert_specific(document, cert);
		if (cert.hasVaccinated()) {
			this.compare_cert_generic(document, cert.v.get(0));
		} else if(cert.hasTested()) {
			this.compare_cert_generic(document, cert.t.get(0));
		} else if(cert.hasRecovered()) {
			this.compare_cert_generic(document, cert.r.get(0));
		} else {
			assertTrue(false, "No Data Found");
		}
	}

	@Test 
	void decode_tested_to_cert() throws IOException, Exception {
		Configuration conf = Configuration.defaultConfiguration();

		// vaccine
		String data = this.read_resource("2.json");
		Object document = conf.jsonProvider().parse(data);
		String prefix = JsonPath.read(document, "$.PREFIX");		

		Cert cert = BarcodeDecodeService.raw_to_cert(prefix);

		// compare Cert content to original json
		this.compare_cert_common(document, cert);
		this.compare_cert_specific(document, cert);
		if (cert.hasVaccinated()) {
			this.compare_cert_generic(document, cert.v.get(0));
		} else if(cert.hasTested()) {
			this.compare_cert_generic(document, cert.t.get(0));
		} else if(cert.hasRecovered()) {
			this.compare_cert_generic(document, cert.r.get(0));
		} else {
			assertTrue(false, "No Data Found");
		}
	}

	@Test 
	void decode_recovered_to_cert() throws Exception {
		Configuration conf = Configuration.defaultConfiguration();

		// vaccine
		String data = this.read_resource("3.json");
		Object document = conf.jsonProvider().parse(data);
		String prefix = JsonPath.read(document, "$.PREFIX");		

		Cert cert = BarcodeDecodeService.raw_to_cert(prefix);

		// compare global values
		// compare Cert content to original json
		this.compare_cert_common(document, cert);
		this.compare_cert_specific(document, cert);
		if (cert.hasVaccinated()) {
			this.compare_cert_generic(document, cert.v.get(0));
		} else if(cert.hasTested()) {
			this.compare_cert_generic(document, cert.t.get(0));
		} else if(cert.hasRecovered()) {
			this.compare_cert_generic(document, cert.r.get(0));
		} else {
			assertTrue(false, "No Data Found");
		}
	}

	@Test
	void decode_all_certs() throws IOException, Exception {
		Configuration conf = Configuration.defaultConfiguration();

		for(int i=1; i<13; i++) {
			System.out.println("Cert: " + i);

			String data = this.read_resource(i+".json");
			Object document = conf.jsonProvider().parse(data);
			String prefix = JsonPath.read(document, "$.PREFIX");		
	
			Cert cert = BarcodeDecodeService.raw_to_cert(prefix);
	
			// compare global values
			// compare Cert content to original json
			this.compare_cert_common(document, cert);
			this.compare_cert_specific(document, cert);
			if (cert.hasVaccinated()) {
				this.compare_cert_generic(document, cert.v.get(0));
			} else if(cert.hasTested()) {
				this.compare_cert_generic(document, cert.t.get(0));
			} else if(cert.hasRecovered()) {
				this.compare_cert_generic(document, cert.r.get(0));
			} else {
				assertTrue(false, "No Data Found");
			}
		}
	}

}
